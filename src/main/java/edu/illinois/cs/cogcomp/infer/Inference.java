package edu.illinois.cs.cogcomp.infer;

public interface Inference<T> {
	T runInference() throws Exception;
}
